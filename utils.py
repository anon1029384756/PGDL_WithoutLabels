import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from typing import Callable

## https://github.com/tensorflow/tensorflow/issues/33478#issuecomment-568290488
def get_call_fn(layer: tf.keras.layers.Layer) -> Callable[[tf.Tensor], tf.Tensor]:
  old_call_fn = layer.call
  def call(input: tf.Tensor) -> tf.Tensor:
    output = old_call_fn(input)
    for hook in layer._hooks:
        hook_result = hook(input, output)
        if hook_result is not None:
          output = hook_result
    return output
  return call

class InputOutputSaver:
  def __call__(self, input: tf.Tensor, output: tf.Tensor) -> None:
    self.input = input
    self.output = output

def diag_avg(x):
    def standard_scale(x):
        return (x - np.mean(x, axis=1, keepdims=True))/(np.std(x, axis=1, keepdims=True))
    x = standard_scale(x)
    return np.diag(x).mean()

def add_gau_noise(x, fac):
    return x + fac * (tf.random.normal(x.shape, mean=tf.math.reduce_mean(x), stddev=tf.math.reduce_std(x)))

def add_uni_noise(x, fac):
    x_n = []
    for w in x:
      x_n.append(np.random.uniform(low=-fac/2, high=fac/2, size=list(w.shape)))
      x_n[-1] *= np.abs(w)
      x_n[-1] += w
    return x_n

def extract_features(model, dataset, batch_size=20, max_batches=250, perturb_input=False, perturb_input_fac=0.08, perturb_model=False, perturb_model_fac=0.0001):

    ## batch the dataset
    batched_ds = iter(dataset.batch(batch_size))

    # pdb.set_trace()
    feature_sets = []
    inputs = []
    labels = []
    num_layers_saved = 0

    for batch_num, (x, y) in enumerate(batched_ds):
        if batch_num == max_batches: break
        for layer in model.layers:
            layer._hooks = []
            layer.call = get_call_fn(layer)
            layer.register_hook = lambda hook: layer._hooks.append(hook)
        savers = {}
        for layer in model.layers:
            saver = InputOutputSaver()
            layer.register_hook(saver)
            savers[layer] = saver
        ## add noise
        if perturb_model == True:
            true_weights = [tf.convert_to_tensor(w) for w in model.get_weights()]
            noisy_weights = add_uni_noise(true_weights, perturb_model_fac)
            model.set_weights(noisy_weights)
        if perturb_input == True:
            x = add_gau_noise(x, perturb_input_fac)
        model(x)
        features = []
        num_layers_saved = 0
        for layer in model.layers:
            if not (isinstance(layer, keras.layers.ReLU) or isinstance(layer, keras.layers.InputLayer)):
                num_layers_saved += 1
                features.append(savers[layer].output)

        ## save extracted feautures
        inputs.append(x.numpy())
        labels.append(y.numpy())
        feature_sets.append([feature.numpy() for feature in features])

    inputs = np.stack(inputs)
    labels = np.stack(labels)
    layer_wise_features = []
    num_batches = len(feature_sets)

    for i in range(num_layers_saved):
        layer_wise_features.append(np.array([feature_set[i] for feature_set in feature_sets]))
    #
    inputs = np.concatenate(inputs,axis=0)
    labels = np.concatenate(labels,axis=0)
    layer_wise_features = [np.concatenate(k,axis=0) for k in layer_wise_features]
    return inputs, labels, layer_wise_features


def create_batches(inputs, labels, layer_wise_features, num_batches):
    num_samples = inputs.shape[0]
    splits = [(i+1)*(num_samples//(num_batches)) for i in range(num_batches-1)]
    batches  = [np.split(k,splits) for k in [inputs, labels, *layer_wise_features]]
    batch_inputs = batches[0]
    batch_labels = batches[1]
    features = list(zip(*batches[2:]))
    batch_features = list(zip(*features))
    return (batch_inputs, batch_labels, batch_features)

def create_train_val_batches(inputs, labels, layer_wise_features, num_val_batches, total_val_split=2):
    ## if  total_val_split == 2, split the data halfway into training batch and multiple validation batches
    num_samples = inputs.shape[0]
    splits = [num_samples - (num_samples//total_val_split)] + [(num_samples - (num_samples//total_val_split) + (i+1)*(num_samples//(total_val_split*num_val_batches))) for i in range(num_val_batches-1)]
    batches  = [np.split(k,splits) for k in [inputs, labels, *layer_wise_features]]
    train_batch_inputs = [batches[0][0]]
    val_batch_inputs = batches[0][1:]
    train_batch_labels = [batches[1][0]]
    val_batch_labels = batches[1][1:]
    features = list(zip(*batches[2:]))
    ## now features[0] contains the training batches layer wise.
    train_batch_features = [[layer_wise_feature] for layer_wise_feature in features[0]] ## single batch for each layer
    val_batch_features = list(list(a) for a in zip(*features[1:])) ## unzip for layer wise

    return (train_batch_inputs, train_batch_labels, train_batch_features), (val_batch_inputs, val_batch_labels, val_batch_features)

def forward_pass(model, x, batchsize=1000):
    if x.shape[0] < batchsize:
        batchsize = x.shape[0]
    iters = x.shape[0] // batchsize
    outs = []
    for i in range(iters):
        outs.append(model(x[i*batchsize:(i+1)*batchsize]))
    
    if (x.shape[0] > batchsize) and (x.shape[0] % batchsize != 0):
        outs.append(model(x[iters*batchsize:]))
    
    outs = tf.concat(outs, axis=0)
    return outs