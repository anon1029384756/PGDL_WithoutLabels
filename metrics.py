import numpy as np
import tensorflow as tf
from tensorflow import keras

import scipy
from sklearn import decomposition
from sklearn import cluster
from sklearn import mixture
from sklearn import metrics

from utils import extract_features, create_train_val_batches, create_batches, forward_pass

def cluster_trace(model, traindata, testdata, with_labels=False):
    """
    Calculates cluster trace metric for a model
    Args:
        model: instance of PGDL model
        traindata: iterable PGDL training dataset
        testdata: iterable PGDL test dataset
        with_labels: if True, uses labels to train clusters, if false, uses model's predictions to train clusters
    Returns:
        train_cluster_trace: Cluster trace calculated on a validation batch from training dataset
        test_cluster_trace: Cluster trace calculated on a validation batch from test dataset
    """
    def combine_clusters(grouped_clusters):
        clust_centers_, clust_labels = zip(*[(k.cluster_centers_, np.repeat(ki,k.cluster_centers_.shape[0])) for ki, k in enumerate(grouped_clusters)])
        clust_centers_ = np.concatenate(clust_centers_,axis=0)
        clust_labels = np.concatenate(clust_labels,axis=0)
        clusters_combined = cluster.KMeans(n_clusters=clust_centers_.shape[0], init=clust_centers_, n_init=1).fit(clust_centers_)
        return clusters_combined, clust_labels
    
    def train_kmeans(train_batch_features, batched_labels, clust_components=3):
        num_classes = len(np.unique(batched_labels[0]))
        actual_labels = batched_labels[0]
        
        x = [train_batch_features[i][0] for i in range(len(train_batch_features))]
        
        x = [xx.reshape(xx.shape[0], -1) for xx in x]
        x_tfms = [decomposition.PCA(0.95).fit(xx) for xx in x]
        x_PCA = [x_tfm.transform(xx) for x_tfm, xx in zip(x_tfms,x)]
        
        clusters = []
        clusters_labels = []
        for pc_PCA in x_PCA:
            clust, clust_lab = combine_clusters([cluster.KMeans(n_clusters=clust_components).fit(pc_PCA[actual_labels == i]) for i in range(num_classes)])
            clusters.append(clust)
            clusters_labels.append(clust_lab)
        
        return x_tfms, clusters, clusters_labels

    def predict_classification(test_batch_features, x_tfms, clusters, clusters_labels):
        x = [test_batch_features[i][0] for i in range(len(test_batch_features))]
        x = [xx.reshape(xx.shape[0], -1) for xx in x]
        x_PCA = [x_tfm.transform(xx) for x_tfm, xx in zip(x_tfms,x)]

        layer_wise_preds = [clusters_labels[i][clusters[i].predict(x_PCA[i])] for i in range(len(clusters))]
        
        return layer_wise_preds
    ## Extract training set intermediate layer features
    inputs, labels, layer_wise_features = extract_features(model, traindata, batch_size=20, max_batches=100)
    num_val_batches = 1
    (train_batch_inputs, train_batch_labels, train_batch_features), (val_batch_inputs, val_batch_labels, val_batch_features) = \
                                create_train_val_batches(inputs, labels, layer_wise_features, num_val_batches)

    ## Extract test set intermediate layer features
    test_inputs, test_labels, test_layer_wise_features = extract_features(model, testdata, batch_size=20, max_batches=50)
    num_test_batches = 1
    (test_batch_inputs, test_batch_labels, test_batch_features) = create_batches(test_inputs, test_labels, test_layer_wise_features, num_test_batches=num_test_batches)
    
    ## training clusters
    clust_components = 3
    if with_labels == False:
        train_preds = forward_pass(model, inputs, batchsize=200)
        train_preds = tf.argmax(train_preds, axis=1).numpy()

        test_preds = forward_pass(model, test_inputs, batchsize=200)
        test_preds = tf.argmax(test_preds, axis=1).numpy()

        x_tfms, clust, clust_lab = train_kmeans(train_batch_features, [train_preds], clust_components=clust_components)
        ## calculate cluster trace
        layer_wise_preds_val = predict_classification(val_batch_features, x_tfms, clust, clust_lab)
        train_cluster_trace = (train_preds[:,None]==layer_wise_preds_val).mean(axis=1).mean()

        layer_wise_preds_test = predict_classification(test_batch_features, x_tfms, clust, clust_lab)
        test_cluster_trace = (test_preds[:,None]==layer_wise_preds_test).mean(axis=1).mean()
    else:
        x_tfms, clust, clust_lab = train_kmeans(train_batch_features, train_batch_labels, clust_components=clust_components)
        ## calculate cluster trace
        layer_wise_preds_val = predict_classification(val_batch_features, x_tfms, clust, clust_lab)
        train_cluster_trace = (val_batch_labels[0][:,None]==layer_wise_preds_val).mean(axis=1).mean()

        layer_wise_preds_test = predict_classification(test_batch_features, x_tfms, clust, clust_lab)
        test_cluster_trace = (test_batch_labels[0][:,None]==layer_wise_preds_test).mean(axis=1).mean()
    
    return train_cluster_trace, test_cluster_trace

def mixup(model, traindata, testdata, with_labels=False):
    """
    Calculates mixup metric for a model
    Args:
        model: instance of PGDL model
        traindata: iterable PGDL training dataset
        testdata: iterable PGDL test dataset
        with_labels: if True, uses labels to generate mixup samples, if false, uses model's predictions to generate mixup samples
    Returns:
        train_mixup_score: Mixup score calculated on a batch from training dataset
        test_mixup_score: Mixup score calculated on a batch from test dataset
    """
    def calculate_mixup_score(model, test_inputs, test_labels, N, preds=None, alpha=0.5):
        
        if preds == None:
            preds = test_labels

        pred_wise_batches = []
        for i in range(test_labels.max()+1):
            pred_wise_batches.append(test_inputs[preds == i])

        pred_wise_mixups = []
        predictions = []
        pred_wise_labels = []
        for i in range(test_labels.max()+1):

            if pred_wise_batches[i].shape[0] <= N:
                continue
            all_indices = []
            for j in range(len(pred_wise_batches[i])):
                interpolating_indices = np.random.choice(np.setdiff1d( np.arange(len(pred_wise_batches[i])), j ),N, replace=False)
                all_indices.append(interpolating_indices)
            all_indices = np.array(all_indices)
            
            #pdb.set_trace()
            int_img = (alpha*pred_wise_batches[i][:,None,:,:,:]) + (1-alpha)*pred_wise_batches[i][all_indices]
            (num_samples,num_int,h,w,c) = int_img.shape
            int_img = int_img.reshape(-1,h,w,c)
            int_pred = forward_pass(model, int_img, batchsize=500)
            int_pred = tf.argmax(int_pred, axis=1).numpy()
            int_pred = int_pred.reshape(num_samples,num_int)
            
            pred_wise_mixups.append(int_pred)
            predictions.append(int_pred.shape[0]*[i])
            pred_wise_labels.append(test_labels[preds == i])

        mixup_score = (pred_wise_labels[:,None]==pred_wise_mixups).mean(axis=1).mean()
        return mixup_score

    batch = next(iter(traindata.repeat(-1).shuffle(5000, seed=42).batch(1000)))
    inputs, labels = batch[0], batch[1]
    test_batch = next(iter(testdata.repeat(-1).shuffle(5000, seed=42).batch(1000)))
    test_inputs, test_labels = test_batch[0], test_batch[1]

    N = 40
    if with_labels == False:
        train_preds = forward_pass(model, inputs, batchsize=200)
        train_preds = tf.argmax(train_preds, axis=1).numpy()

        test_preds = forward_pass(model, test_inputs, batchsize=200)
        test_preds = tf.argmax(test_preds, axis=1).numpy()
    else:
        train_preds = None
        test_preds = None
    
    train_mixup_score = calculate_mixup_score(model, inputs, labels, N, preds=train_preds)
    test_mixup_score = calculate_mixup_score(model, test_inputs, test_labels, N, preds=test_preds)

    return train_mixup_score, test_mixup_score


def roughness(model, traindata, testdata):
    """
    Calculates roughness metric for a model
    Args:
        model: instance of PGDL model
        traindata: iterable PGDL training dataset
        testdata: iterable PGDL test dataset
    Returns:
        val_roughness: Roughness metric calculated on a validation batch from training dataset
        test_roughness: Roughness metric calculated on a validation batch from test dataset
    """
    def train_kmeans(train_batch_features):
        x = [train_batch_features[i][0] for i in range(len(train_batch_features)-1)]
        y = [train_batch_features[i+1][0] for i in range(len(train_batch_features)-1)]
        
        y = [yy.reshape(yy.shape[0], -1) for yy in y]
        x = [xx.reshape(xx.shape[0], -1) for xx in x]
        
        y_tfms = [decomposition.PCA(n_components=min(250,yy.shape[-1])).fit(yy) for yy in y]
        y_PCA = [y_tfm.transform(yy) for y_tfm, yy in zip(y_tfms,y)]
        
        clusters = [cluster.KMeans(n_clusters=5).fit(yy_PCA) for yy_PCA in y_PCA]
        
        x_tfms = [decomposition.PCA(n_components=min(250,xx.shape[-1])).fit(xx) for xx in x]
        
        return x_tfms, y_tfms, clusters
    
    def calculate_layer_roughness(val_batch_features, x_tfms, y_tfms, clusters):
        """
        Measure the db score of input clusters on a validation batch, colored on output feature groups of the training batch, for each layer
        """
        dists = []
        for batch in range(len(val_batch_features[0])):
            x = [val_batch_features[i][batch] for i in range(len(val_batch_features)-1)]
            y = [val_batch_features[i+1][batch] for i in range(len(val_batch_features)-1)]
            
            y = [yy.reshape(yy.shape[0], -1) for yy in y]
            x = [xx.reshape(xx.shape[0], -1) for xx in x]
            
            y_PCA = [y_tfm.transform(yy) for y_tfm, yy in zip(y_tfms,y)]
            y_group_labels = [c.predict(yy_PCA) for c, yy_PCA in zip(clusters,y_PCA)]
            
            x_PCA = [x_tfm.transform(xx) for x_tfm, xx in zip(x_tfms,x)]

            dbs = [metrics.davies_bouldin_score(xx_PCA, lab) for xx_PCA,lab in zip(x_PCA, y_group_labels)]
            dists.append(dbs)
        return dists

    ## Extract training set intermediate layer features
    inputs, labels, layer_wise_features = extract_features(model, traindata, batch_size=20, max_batches=100)
    num_val_batches = 1
    (train_batch_inputs, train_batch_labels, train_batch_features), (val_batch_inputs, val_batch_labels, val_batch_features) = \
                                create_train_val_batches(inputs, labels, layer_wise_features, num_val_batches)
    
    ## training clusters
    x_tfms, y_tfms, clusters = train_kmeans(train_batch_features)
    
    ## calculate roughness using DB
    dists = calculate_layer_roughness(val_batch_features, x_tfms, y_tfms, clusters)
    val_roughness = dists.mean()
    
    ## Extract test set intermediate layer features
    test_inputs, test_labels, test_layer_wise_features = extract_features(model, testdata, batch_size=20, max_batches=50)
    num_test_batches = 1
    (test_batch_inputs, test_batch_labels, test_batch_features) = create_batches(test_inputs, test_labels, test_layer_wise_features, num_test_batches=num_test_batches)

    ## calculate roughness using DB
    test_dists = calculate_layer_roughness(test_batch_features, x_tfms, y_tfms, clusters)
    test_roughness = test_dists.mean()
    
    return val_roughness, test_roughness


def confidence(model, traindata, testdata):
    """
    Calculates confidence metric for a model
    Args:
        model: instance of PGDL model
        traindata: iterable PGDL training dataset
        testdata: iterable PGDL test dataset
    Returns:
        val_confidence: Confidence metric calculated on a validation batch from training dataset
        test_confidence: Confidence metric calculated on a validation batch from test dataset
    """
    def train_kmeans_with_gmm(train_batch_features):
        x = [train_batch_features[i][0] for i in range(len(train_batch_features)-1)]
        y = [train_batch_features[i+1][0] for i in range(len(train_batch_features)-1)]
        
        y = [yy.reshape(yy.shape[0], -1) for yy in y]
        x = [xx.reshape(xx.shape[0], -1) for xx in x]
        
        y_tfms = [decomposition.PCA(n_components=3).fit(yy) for yy in y]
        y_PCA = [y_tfm.transform(yy) for y_tfm, yy in zip(y_tfms,y)]
        
        clusters = [cluster.KMeans(n_clusters=5).fit(yy_PCA) for yy_PCA in y_PCA]
        
        x_tfms = [decomposition.PCA(n_components=3).fit(xx) for xx in x]
        x_PCA = [x_tfm.transform(xx) for x_tfm, xx in zip(x_tfms,x)]

        mixtures = []
        for x_PCA_layer, c in zip(x_PCA, clusters):
            num_groups = len(np.unique(c.labels_))

            group_labels = c.labels_
            if x_PCA_layer.shape[0] != group_labels.shape[0]: # conv layer to dense layer for pixel wise roughness
                group_labels = np.repeat(group_labels, x_PCA_layer.shape[0] // group_labels.shape[0] )
            
            mixtures_layer = []
            for i in range(num_groups):
                if x_PCA_layer[group_labels == i].shape[0] >= 3: ## minimum number of samples in a group to fit 3 micture components
                    mixtures_layer.append(mixture.GaussianMixture(n_components=3).fit(x_PCA_layer[group_labels == i]))
            mixtures.append(mixtures_layer)
        
        return x_tfms, y_tfms, clusters, mixtures

    def layerwise_sample_confidences(val_batch_features, x_tfms, y_tfms, clusters, mixtures, batch_size):
        confidences_all_batches = []
        for batch in range(len(val_batch_features[0])):
            x_val = [val_batch_features[i][batch] for i in range(len(val_batch_features)-1)]
            y_val = [val_batch_features[i+1][batch] for i in range(len(val_batch_features)-1)]

            y_val = [yy.reshape(yy.shape[0], -1) for yy in y_val]
            x_val = [xx.reshape(xx.shape[0], -1) for xx in x_val]

            y_PCA_val = [y_tfm.transform(yy) for y_tfm, yy in zip(y_tfms,y_val)]

            x_PCA_val = [x_tfm.transform(xx) for x_tfm, xx in zip(x_tfms,x_val)]

            confs = []
            for lyr in range(len(x_val)):

                scores = np.stack([m.score_samples(x_PCA_val[lyr]) for m in mixtures[lyr]], axis=1)
                scores = scipy.special.softmax(scores, axis=1).max(axis=1)

                ## weight the likelihoods with layer's output
                if scores.shape[0] == y_PCA_val[lyr].shape[0]: ## num of "pixelwise" samples change for flatten layer.
                    scores = scores * np.linalg.norm(y_PCA_val[lyr], axis=1)
                scores = scores.reshape(batch_size,-1).mean(axis=1)
                confs.append(scores)
                
            confidences_all_batches.append(confs)
        confidences_all_batches = np.array(confidences_all_batches)
        confidences_all_batches = confidences_all_batches.mean(axis=0) ## avg over batches
        return confidences_all_batches

    ## Extract training set intermediate layer features
    inputs, labels, layer_wise_features = extract_features(model, traindata, batch_size=20, max_batches=100)
    num_val_batches = 1
    (train_batch_inputs, train_batch_labels, train_batch_features), (val_batch_inputs, val_batch_labels, val_batch_features) = \
                                create_train_val_batches(inputs, labels, layer_wise_features, num_val_batches)
    
    ## training clusters
    x_tfms, y_tfms, clusters, mixtures = train_kmeans_with_gmm(train_batch_features)
    
    ## calculate confidence
    confs = layerwise_sample_confidences(val_batch_features, x_tfms, y_tfms, clusters, mixtures, batch_size=val_batch_inputs[0].shape[0])
    val_confidence = confs.mean()
    
    ## Extract test set intermediate layer features
    test_inputs, test_labels, test_layer_wise_features = extract_features(model, testdata, batch_size=20, max_batches=50)
    num_test_batches = 1
    (test_batch_inputs, test_batch_labels, test_batch_features) = create_batches(test_inputs, test_labels, test_layer_wise_features, num_test_batches=num_test_batches)

    ## calculate confidence
    test_confs = layerwise_sample_confidences(test_batch_features, x_tfms, y_tfms, clusters, mixtures, batch_size=test_batch_inputs[0].shape[0])
    test_confidence = test_confs.mean()
    
    return val_confidence, test_confidence